package core.appManager;

import org.openqa.selenium.WebDriver;

import main.main_android.Panels.HeadCheckManager_android;
import main.main_ios.Panels.HeadCheckManager_ios;
import main.main_web.Panels.CraigslistManager;



/**
 * Manages all existing apps Abstract driver refers to this manager to call
 * methods in different apps
 */
public class AppManager extends AppPage {

	public AppManager(WebDriver driver) {
		super(driver);

	}

	public CraigslistManager cr = new CraigslistManager(getWebDriver());
	public HeadCheckManager_ios hc_ios = new HeadCheckManager_ios(getWebDriver());
	public HeadCheckManager_android hc_android = new HeadCheckManager_android(getWebDriver());
}