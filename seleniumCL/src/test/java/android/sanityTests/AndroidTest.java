package test.java.android.sanityTests;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import core.driver.AbstractDriver;
import core.driver.TestLog;
import core.runner.ParallelRunner;
import main.main_android.categories.login;
import main.main_android.categories.user;
import main.main_android.constants.UserInfo;
import main.main_android.objects.UserObject;

@RunWith(ParallelRunner.class)
public class AndroidTest extends AbstractDriver {

	@Before
	public void beforeMethod() throws Exception {
		setupWebDriver();
	}

	@Category({ login.class, user.class })
	@Test
	public void androidTest() {
		UserObject user = new UserObject().withUsername(UserInfo.USER_NAME).withPassword(UserInfo.PASSWORD);
		
		TestLog.When("I login with default user");
		app.hc_android.login.login(user);
	}
}