package test.java.web.sanityTests;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import core.driver.AbstractDriver;
import core.driver.TestLog;
import core.helpers.Helper;
import core.runner.ParallelRunner;
import main.main_web.categories.login;
import main.main_web.categories.user;
import main.main_web.constants.UserInfo;
import main.main_web.objects.UserObject;
import main.main_web.pages.CraigsList;

@RunWith(ParallelRunner.class)
public class verifyLoginTest extends AbstractDriver {

	@Before
	public void beforeMethod() throws Exception {
		setupWebDriver(CraigsList.CRAISTLIST);
	}

	@Category({ login.class, user.class })
    @Test()
	public void validateUserLogin() {
		UserObject user = new UserObject().withEmail(UserInfo.USER_NAME).withPassword(UserInfo.PASSWORD);
		
		TestLog.When("I login with default user");
		app.cr.login.login(user);
		
		TestLog.Then("I verify the account header is displayed");
		Helper.verifyElementIsDisplayed(app.cr.postings.byAccountHeader());
	}

	@Category({ login.class, user.class })
    @Test
	public void invalidEmailLogin() {
		UserObject user = new UserObject().withEmail("invalid").withPassword(UserInfo.PASSWORD);
		
		TestLog.When("I login with invalid email");
		app.cr.login.login_invalid(user);
		
		TestLog.Then("I am able to view alert");
		app.cr.login.verifyAlert("Your email address, handle, or password is incorrect");
	}

	@Category({ login.class, user.class })
	@Test
	public void invalidPasswordLogin() {
		UserObject user = new UserObject().withEmail(UserInfo.USER_NAME).withPassword("invalid");
		
		TestLog.When("I login with invalid password");
		app.cr.login.login_invalid(user);
		
		TestLog.Then("I am able to view alert");
		app.cr.login.verifyAlert("Your email address, handle, or password is incorrect");
	}

}