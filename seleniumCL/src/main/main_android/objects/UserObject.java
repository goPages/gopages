package main.main_android.objects;

public class UserObject {

	public String email;
	public String username;
	public String password;

	public UserObject withEmail(String email) {
		this.email = email;
		return this;
	}

	public UserObject withUsername(String username) {
		this.username = username;
		return this;
	}

	public UserObject withPassword(String password) {
		this.password = password;
		return this;
	}
}