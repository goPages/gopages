package main.main_android.Panels;

import org.openqa.selenium.WebDriver;

import core.driver.EnhancedBy;
import core.helpers.FormHelper;
import core.helpers.Helper;
import io.appium.java_client.MobileBy;
import main.main_android.objects.UserObject;
import main.main_android.pages.HeadCheck;

public class LoginPanel extends HeadCheck {

	HeadCheckManager_android manager;

	public LoginPanel(WebDriver driver, HeadCheckManager_android manager) {
		super(driver);
		this.manager = manager;

	}

	private static String app_package_name = "com.headcheckhealth.headcheck:id/";
	private static final String USER_ID = app_package_name + "login_username";
	private static final String PASSWORD_FIELD = app_package_name + "login_password";
	private static final String LOGIN_BUTTON = app_package_name + "login_button";

	public EnhancedBy byUserName() {
		return BySelector(MobileBy.id(USER_ID), "user field");
	}

	public EnhancedBy byPasswordField() {
		return BySelector(MobileBy.id(PASSWORD_FIELD), "password field");
	}

	public EnhancedBy byLoginButton() {
		return BySelector(MobileBy.id(LOGIN_BUTTON), "submit");
	}

	/**
	 * enter login info and click login button
	 * 
	 * @param user
	 */
	public void login(UserObject user) {
		Helper.clickAndExpect(byLoginButton(), byUserName());
		FormHelper.setField(user.username, byUserName());
		FormHelper.setField(user.password, byPasswordField());
		// FormHelper.formSubmit(byLoginButton(), byHeadCheckLogo());
	}
}