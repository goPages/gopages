package main.main_ios.Panels.Tests.ElementTests;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import core.driver.AbstractDriver;
import core.helpers.Helper;
import core.runner.ParallelRunner;

@RunWith(ParallelRunner.class)
public class LoginPanelTest extends AbstractDriver {

	@Before
	public void beforeMethod() throws Exception {
		setupWebDriver();
	}

	@Test
	public void VerifyLoginPanel() {
		Helper.verifyElementIsDisplayed(app.cr.login.byMyAccount());
		Helper.clickAndExpect(app.cr.login.byMyAccount(), app.cr.login.byPasswordField());
		Helper.verifyElementIsDisplayed(app.cr.login.byPasswordField());
		Helper.verifyElementIsDisplayed(app.cr.login.byEmailField());
		Helper.verifyElementIsDisplayed(app.cr.login.byLoginSubmit());
	}
}