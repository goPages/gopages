package main.main_ios.Panels;

import org.openqa.selenium.WebDriver;

import core.driver.EnhancedBy;
import core.helpers.FormHelper;
import io.appium.java_client.MobileBy;
import main.main_ios.objects.UserObject;
import main.main_ios.pages.HeadCheck;

public class LoginPanel extends HeadCheck {

	HeadCheckManager_ios manager;

	public LoginPanel(WebDriver driver, HeadCheckManager_ios manager) {
		super(driver);
		this.manager = manager;

	}

	private static final String EMAIL_FIELD = "XCUIElementTypeTextField";
	private static final String PASSWORD_FIELD = "XCUIElementTypeSecureTextField";
	private static final String LOGIN_SUBMIT = "//XCUIElementTypeButton[@name='Login']";
	private static final String QUICK_TEST = "//XCUIElementTypeButton[@name='Quick Test']";
	private static final String REGISTER = "//XCUIElementTypeButton[@name='Register']";
	private static final String HEADCHECK_LOGO = "//XCUIElementTypeImage[@name='register_logo']";

	public EnhancedBy byEmailField() {
		return BySelector(MobileBy.className(EMAIL_FIELD), "email field");
	}

	public EnhancedBy byPasswordField() {
		return BySelector(MobileBy.className(PASSWORD_FIELD), "password field");
	}

	public EnhancedBy byLoginSubmit() {
		return BySelector(MobileBy.xpath(LOGIN_SUBMIT), "submit");
	}

	public EnhancedBy byQuickTest() {
		return BySelector(MobileBy.xpath(QUICK_TEST), "quick test");
	}

	public EnhancedBy byRegister() {
		return BySelector(MobileBy.xpath(REGISTER), "register");
	}

	public EnhancedBy byHeadCheckLogo() {
		return BySelector(MobileBy.xpath(HEADCHECK_LOGO), "head check logo");
	}

	/**
	 * enter login info and click login button
	 * 
	 * @param user
	 */
	public void login(UserObject user) {
		FormHelper.setField(user.username, byEmailField());
		FormHelper.setField(user.password, byPasswordField());
		//FormHelper.formSubmit(byLoginSubmit(), byHeadCheckLogo());
	}
}