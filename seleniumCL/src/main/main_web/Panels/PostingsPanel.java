package main.main_web.Panels;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import core.driver.EnhancedBy;
import main.main_web.pages.CraigsList;

public class PostingsPanel extends CraigsList {

	CraigslistManager manager;

	public PostingsPanel(WebDriver driver, CraigslistManager manager) {
		super(driver);
		this.manager = manager;
	}

	private final String ACCOUNT_HEADER = ".account-header";
	private final String SECTION_TITLE = ".crumb.section";

	// Postings form
	private final String NEW_POST_DROPDOWN = ".new_posting_thing [name='areaabb']";
	private final String NEW_POST_DROPDOWN_OPTIONS = ".new_posting_thing [name='areaabb'] option";
	private final String NEW_POST_SUBMIT = ".new_posting_thing [type='submit']";
	private final String POSTING_TYPE = ".selection-list li";
	private final String CATEGORY = ".selection-list li";
	private final String SUB_LOCATION = ".selection-list li";
	private final String POSTING_TITLE = "#PostingTitle";
	private final String POSTING_CODE = ".postal-code #postal_code";
	private final String POSTING_BODY = "#PostingBody";
	private final String POSTING_CONTINUE_BUTTON = ".bigbutton";
	private final String POSTING_MAP = "#map";
	private final String POSTING_UPLOADER = "#uploader";
	private final String POSTING_PUBLISH_BAR = "#publish_top";
	private final String POSTING_PUBLISH_BUTTON = ".button[type='submit']";
	private final String POSTING_IMAGE_UPLOADER = "[type='file']";
	private final String POSTING_IMAGE_WRAP = ".imgwrap img";
	private final String POSTING_EMAIL_CONFIRMATION = ".e";

	public EnhancedBy byAccountHeader() {
		return BySelector(By.cssSelector(ACCOUNT_HEADER), "account panel");
	}

	public EnhancedBy bySectionTitle() {
		return BySelector(By.cssSelector(SECTION_TITLE), "section title");
	}

	public EnhancedBy byNewPostDropdown() {
		return BySelector(By.cssSelector(NEW_POST_DROPDOWN), "new post list");
	}

	public EnhancedBy byNewPostDropDownOptions() {
		return BySelector(By.cssSelector(NEW_POST_DROPDOWN_OPTIONS), "new post options");
	}

	public EnhancedBy byPostSubmit() {
		return BySelector(By.cssSelector(NEW_POST_SUBMIT), "new post submit ");
	}

	public EnhancedBy byPostType() {
		return BySelector(By.cssSelector(POSTING_TYPE), "posting type");
	}

	public EnhancedBy byCategory() {
		return BySelector(By.cssSelector(CATEGORY), "posting category");
	}

	public EnhancedBy bySubLocation() {
		return BySelector(By.cssSelector(SUB_LOCATION), "sub location");
	}

	public EnhancedBy byPostingTitle() {
		return BySelector(By.cssSelector(POSTING_TITLE), "posting title");
	}

	public EnhancedBy byPostingCode() {
		return BySelector(By.cssSelector(POSTING_CODE), "posting code");
	}

	public EnhancedBy byPostingBody() {
		return BySelector(By.cssSelector(POSTING_BODY), "posting body");
	}

	public EnhancedBy byPostingContinueButton() {
		return BySelector(By.cssSelector(POSTING_CONTINUE_BUTTON), "posting continue botton");
	}

	public EnhancedBy byPostingMap() {
		return BySelector(By.cssSelector(POSTING_MAP), "posting map");
	}

	public EnhancedBy byPostingUploader() {
		return BySelector(By.cssSelector(POSTING_UPLOADER), "posting uploaded");
	}

	public EnhancedBy byPostingPublishBar() {
		return BySelector(By.cssSelector(POSTING_PUBLISH_BAR), "posting publish bar");
	}

	public EnhancedBy byPostingPublishButton() {
		return BySelector(By.cssSelector(POSTING_PUBLISH_BUTTON), "posting publish button");
	}

	public EnhancedBy byImageUploader() {
		return BySelector(By.cssSelector(POSTING_IMAGE_UPLOADER), "posting publish button");
	}

	public EnhancedBy byImageWrap() {
		return BySelector(By.cssSelector(POSTING_IMAGE_WRAP), "posting image wrap");
	}

	public EnhancedBy byEmailvalue() {
		return BySelector(By.cssSelector(POSTING_EMAIL_CONFIRMATION), "email confirmation");
	}

}