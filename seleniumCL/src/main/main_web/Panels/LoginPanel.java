package main.main_web.Panels;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import core.driver.EnhancedBy;
import core.helpers.AssertHelper;
import core.helpers.FormHelper;
import core.helpers.Helper;
import main.main_web.objects.UserObject;
import main.main_web.pages.CraigsList;

public class LoginPanel extends CraigsList {

	CraigslistManager manager;

	public LoginPanel(WebDriver driver, CraigslistManager manager) {
		super(driver);
		this.manager = manager;

	}

	private static final String MY_ACCOUNT = "#postlks [href*='accounts']";
	private static final String EMAIL_FIELD = "#inputEmailHandle";
	private static final String PASSWORD_FIELD = "#inputPassword";
	private static final String LOGIN_SUBMIT = ".login-box .accountform-btn";
	private static final String SUCCESS_CONFIRMATION = ".login-box .accountform-btn";
	private static final String ERROR_ALERT = ".alert-error";
	
	
	public EnhancedBy byMyAccount() {
		return BySelector(By.cssSelector(MY_ACCOUNT), "my account link");
	}

	public EnhancedBy byEmailField() {
		return BySelector(By.cssSelector(EMAIL_FIELD), "email field");
	}

	public EnhancedBy byPasswordField() {
		return BySelector(By.cssSelector(PASSWORD_FIELD), "password field");
	}

	public EnhancedBy byLoginSubmit() {
		return BySelector(By.cssSelector(LOGIN_SUBMIT), "submit button");
	}

	public EnhancedBy bySuccessConfirmation() {
		return BySelector(By.cssSelector(SUCCESS_CONFIRMATION), "submit button");
	}
	
	public EnhancedBy byErrorAlert() {
		return BySelector(By.cssSelector(ERROR_ALERT), "error alert");
	}

	/**
	 * enter login info and click login button
	 * 
	 * @param user
	 */
	public void login(UserObject user) {
		setLoginFields(user);
		FormHelper.formSubmit(byLoginSubmit(), manager.postings.byAccountHeader());
		
	}
	
	public void login_invalid(UserObject user) {
		setLoginFields(user);
		FormHelper.formSubmit(byLoginSubmit(), byLoginSubmit());
	}
	
	public void verifyAlert(String alert){
		String error = "Your email address, handle, or password is incorrect"; 
		AssertHelper.assertTrue("", error.contains(alert));
	}
	
	public void setLoginFields(UserObject user) {
		Helper.clickAndExpect(byMyAccount(), byPasswordField());
		FormHelper.setField(user.email, byEmailField());
		FormHelper.setField(user.password, byPasswordField());
	}
}