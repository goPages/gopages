package main.main_web.Panels.Tests.ElementTests;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import core.driver.AbstractDriver;
import core.helpers.Helper;
import core.runner.ParallelRunner;
import main.main_web.pages.CraigsList;

@RunWith(ParallelRunner.class)
public class LoginPanelTest extends AbstractDriver {

	@Before
	public void beforeMethod() throws Exception {
		setupWebDriver(CraigsList.CRAISTLIST);
	}

	@Test
	public void VerifyLoginPanel() {
		Helper.verifyElementIsDisplayed(app.cr.login.byMyAccount());
		Helper.clickAndExpect(app.cr.login.byMyAccount(), app.cr.login.byPasswordField());
		Helper.verifyElementIsDisplayed(app.cr.login.byPasswordField());
		Helper.verifyElementIsDisplayed(app.cr.login.byEmailField());
		Helper.verifyElementIsDisplayed(app.cr.login.byLoginSubmit());
	}

}