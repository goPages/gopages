package main.main_web.Panels;

import org.openqa.selenium.WebDriver;

import main.main_web.pages.CraigsList;

public class CraigslistManager extends CraigsList {

	public CraigslistManager(WebDriver driver) {
		super(driver);

	}

	public LoginPanel login = new LoginPanel(getWebDriver(), this);
	public PostingsPanel postings = new PostingsPanel(getWebDriver(), this);

}